use clap::Parser;

#[derive(Parser)]
#[command(version = "0.0.2", about, long_about = None)]
struct Cli {
    #[arg(short, long)]
    verbose: bool,

    /// Repeats the output
    #[arg(short, long, default_value_t = 1)]
    repeat: u64,

    text: Option<String>,
}

fn main() {
    let cli = Cli::parse();
    if let Some(output) = cli.text.as_deref() {
        println!("{}", repeat_string(output.to_string(), cli.repeat))
    } else {
        eprintln!("Please provide text!")
    }
}

fn repeat_string(input: String, repeat: u64) -> String {
    let mut output = input.clone();
    if repeat > 1 {
        for _ in 1..repeat {
            output = format!("{}\n{}", output, input)
        }
    }
    output
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_repeat() {
        let mut expected = String::from("test");
        let mut actual = repeat_string(String::from("test"), 1);
        assert_eq!(actual, expected);

        expected = String::from("test\ntest");
        actual = repeat_string(String::from("test"), 2);
        assert_eq!(actual, expected);

        expected = String::from("test\ntest\ntest");
        actual = repeat_string(String::from("test"), 3);
        assert_eq!(actual, expected);
    }

    #[test]
    fn test_spaces() {
        let mut expected = String::from("test test");
        let mut actual = repeat_string(String::from("test test"), 1);
        assert_eq!(actual, expected);

        expected = String::from("test test\ntest test");
        actual = repeat_string(String::from("test test"), 2);
        assert_eq!(actual, expected);

        expected = String::from("test test\ntest test\ntest test");
        actual = repeat_string(String::from("test test"), 3);
        assert_eq!(actual, expected);

        expected = String::from(" test test");
        actual = repeat_string(String::from(" test test"), 1);
        assert_eq!(actual, expected);

        expected = String::from(" test test ");
        actual = repeat_string(String::from(" test test "), 1);
        assert_eq!(actual, expected);

        expected = String::from("test test ");
        actual = repeat_string(String::from("test test "), 1);
        assert_eq!(actual, expected);
    }
}
